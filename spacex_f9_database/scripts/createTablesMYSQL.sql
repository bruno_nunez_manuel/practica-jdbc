DROP SCHEMA IF EXISTS spacex_f9;
CREATE SCHEMA spacex_f9 COLLATE = utf8_general_ci;
USE spacex_f9;


CREATE TABLE f9_booster (
    booster_id CHAR(5), /*Absurdo que en HR usasen NOT NULL en una PK?*/
    first_static_fire DATE CHECK (first_static_fire <= SYSDATE()),
    times_flown INT UNSIGNED NOT NULL DEFAULT 0,
    is_operative BOOL NOT NULL DEFAULT FALSE,
    PRIMARY KEY (booster_id)
);

CREATE TABLE landing_site (
    landing_site_id VARCHAR(5),
    name VARCHAR(100) NOT NULL CHECK (name != ''),
    land_or_sea ENUM ('land', 'sea') NOT NULL,
    is_operative BOOL DEFAULT FALSE,
    PRIMARY KEY (landing_site_id)
);

CREATE TABLE landing (
    landing_site_id VARCHAR(5),
    booster_id CHAR(5),
    landing_date DATE CHECK (landing_date <= SYSDATE()),
    FOREIGN KEY (landing_site_id)
        REFERENCES landing_site (landing_site_id),
    FOREIGN KEY (booster_id)
        REFERENCES f9_booster (booster_id),
    PRIMARY KEY (landing_site_id, booster_id, landing_date)
);

CREATE TABLE mission (
    mission_id INT AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL CHECK (name != ''),
    payload_kg DOUBLE UNSIGNED NOT NULL,
    altitude_km INT UNSIGNED NOT NULL,
    flight_date DATE CHECK (flight_date <= SYSDATE()),
    booster_id CHAR(5),
    PRIMARY KEY (mission_id),
    FOREIGN KEY (booster_id)
        REFERENCES f9_booster (booster_id)
);


