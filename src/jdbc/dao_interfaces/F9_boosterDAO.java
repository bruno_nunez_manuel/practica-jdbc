package jdbc.dao_interfaces;

import jdbc.models.F9Booster;

import java.sql.Date;
import java.util.List;

public interface F9_boosterDAO {

    boolean insertF9Booster(
            String booster_id,
            Date first_static_fire,
            int times_flown,
            boolean is_operative
    );

    List<F9Booster> selectAllF9Boosters();

    public boolean updateF9BoosterByID(
            String booster_id,
            Date first_static_fire,
            int times_flown,
            boolean is_operative
    );

    boolean deleteF9BoosterByID(String booster_id);

}
