package jdbc.dao_interfaces;

import jdbc.models.Mission;

import java.sql.Date;
import java.util.List;

public interface MissionDAO {

    boolean insertMission(
            int mission_id,
            String name,
            double payload_kg,
            int altitude_km,
            Date flight_date,
            String booster_id
    );

    List<Mission> selectAllMissions();

    boolean updateMissionByID(
            int mission_id,
            String name,
            double payload_kg,
            int altitude_km,
            Date flight_date,
            String booster_id
    );

    boolean deleteMissionByID(int mission_id);

}
