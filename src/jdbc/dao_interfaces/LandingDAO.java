package jdbc.dao_interfaces;

import jdbc.models.F9Booster;
import jdbc.models.Landing;

import java.sql.Date;
import java.util.List;


public interface LandingDAO {

    boolean insertLanding(
            String landing_site_id,
            String booster_id,
            Date landing_date
    );

    List<Landing> selectAllLandings();

    boolean deleteLanding(
            String landing_site_id,
            String booster_id,
            Date landing_date
    );

}
