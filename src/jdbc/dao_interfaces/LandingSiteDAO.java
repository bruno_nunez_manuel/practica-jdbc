package jdbc.dao_interfaces;


import jdbc.models.LandingSite;

import java.sql.ResultSet;
import java.util.List;


public interface LandingSiteDAO {

    boolean insertLandingSite(
            String landing_site_id,
            String name,
            String land_or_sea,
            boolean is_operative
    );

    List<LandingSite> selectAllLandingSites();

    boolean updateLandingSiteByID(
            String landing_site_id,
            String name,
            String land_or_sea,
            boolean is_operative
    );

    boolean deleteLandingSiteByID(String landing_site_id);

}
