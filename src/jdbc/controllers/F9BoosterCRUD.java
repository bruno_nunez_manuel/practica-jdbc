package jdbc.controllers;

import jdbc.dao_interfaces.F9_boosterDAO;
import jdbc.models.F9Booster;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class F9BoosterCRUD implements F9_boosterDAO {
    private final Connection connection;
    private final String INSERT = "INSERT INTO f9_booster (" +
            "booster_id, first_static_fire, times_flown, is_operative) " +
            "VALUES (?, ?, ?, ?)";
    private final String SELECT = "SELECT * FROM f9_booster";
    private final String UPDATE = "UPDATE f9_booster SET booster_id=?," +
            " first_static_fire=?, times_flown=?, is_operative=? " +
            "WHERE booster_id=?";
    private final String DELETE = "DELETE FROM f9_booster WHERE booster_id=?";

    public F9BoosterCRUD(Connection connection) {
        this.connection = connection;
    }

    @Override
    public boolean insertF9Booster(
            String booster_id,
            Date first_static_fire,
            int times_flown,
            boolean is_operative)
    {
        boolean inserted = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, booster_id);
            preparedStatement.setDate(2, first_static_fire);
            preparedStatement.setDouble(3, times_flown);
            preparedStatement.setBoolean(4, is_operative);
            if (preparedStatement.executeUpdate() == 1) inserted = true;
            preparedStatement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return inserted;
    }

    @Override
    public List<F9Booster> selectAllF9Boosters() {
        List<F9Booster> boosters = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                F9Booster f9Booster = new F9Booster(
                        resultSet.getString("booster_id"),
                        resultSet.getDate("first_static_fire"),
                        resultSet.getInt("times_flown"),
                        resultSet.getBoolean("is_operative")
                );
                boosters.add(f9Booster);
            }
            preparedStatement.close();
            resultSet.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return boosters;
    }

    @Override
    public boolean updateF9BoosterByID(
            String booster_id,
            Date first_static_fire,
            int times_flown,
            boolean is_operative)
    {
        boolean updated = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, booster_id);
            preparedStatement.setDate(2, first_static_fire);
            preparedStatement.setInt(3, times_flown);
            preparedStatement.setBoolean(4, is_operative);
            preparedStatement.setString(5, booster_id);
            if (preparedStatement.executeUpdate() == 1) updated = true;
            preparedStatement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return updated;
    }

    @Override
    public boolean deleteF9BoosterByID(String booster_id) {
        boolean deleted = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setString(1, booster_id);
            if (preparedStatement.executeUpdate() == 1) deleted = true;
            preparedStatement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return deleted;
    }

}
