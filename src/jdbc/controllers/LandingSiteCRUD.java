package jdbc.controllers;

import jdbc.dao_interfaces.LandingSiteDAO;
import jdbc.models.LandingSite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class LandingSiteCRUD implements LandingSiteDAO {
    private final Connection connection;
    private final String INSERT = "INSERT INTO landing_site (" +
            "landing_site_id, name, land_or_sea, is_operative) " +
            "VALUES (?, ?, ?, ?)";
    private final String SELECT_ALL = "SELECT * FROM landing_site";
    private final String UPDATE = "UPDATE landing_site " +
            "SET landing_site_id=?, name=?, land_or_sea=?, is_operative=? " +
            "WHERE landing_site_id=?";
    private final String DELETE = "DELETE FROM landing_site WHERE landing_site_id=?";

    public LandingSiteCRUD(Connection connection) {
        this.connection = connection;
    }

    @Override
    public boolean insertLandingSite(
            String landing_site_id,
            String name,
            String land_or_sea,
            boolean is_operative)
    {
        boolean inserted = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, landing_site_id);
            preparedStatement.setString(2, name);
            preparedStatement.setString(3, land_or_sea);
            preparedStatement.setBoolean(4, is_operative);
            if (preparedStatement.executeUpdate() == 1) inserted = true;
            preparedStatement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return inserted;
    }

    @Override
    public List<LandingSite> selectAllLandingSites() {
        List<LandingSite> landing_sites= new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                LandingSite landingSite = new LandingSite(
                        resultSet.getString("landing_site_id"),
                        resultSet.getString("name"),
                        resultSet.getString("land_or_sea"),
                        resultSet.getBoolean("is_operative")
                );
                landing_sites.add(landingSite);
            }
            preparedStatement.close();
            resultSet.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return landing_sites;
    }

    @Override
    public boolean updateLandingSiteByID(
            String landing_site_id,
            String name,
            String land_or_sea,
            boolean is_operative
    ) {
        boolean updated = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, landing_site_id);
            preparedStatement.setString(2, name);
            preparedStatement.setString(3, land_or_sea);
            preparedStatement.setBoolean(4, is_operative);
            preparedStatement.setString(5, landing_site_id);
            if (preparedStatement.executeUpdate() == 1) updated = true;
            preparedStatement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return updated;
    }

    @Override
    public boolean deleteLandingSiteByID(String landing_site_id) {
        boolean deleted = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setString(1, landing_site_id);
            if (preparedStatement.executeUpdate() == 1) deleted = true;
            preparedStatement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return deleted;
    }

}
