package jdbc.controllers;

import jdbc.dao_interfaces.LandingDAO;
import jdbc.models.Landing;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LandingCRUD implements LandingDAO {
    private final Connection connection;
    private final String INSERT = "INSERT INTO landing (" +
            "landing_site_id, booster_id, landing_date) " +
            "VALUES (?, ?, ?)";
    private final String SELECT = "SELECT * FROM landing";
    private final String UPDATE = "UPDATE landing SET booster_id=?," +
            " first_static_fire=?, times_flown=?, is_operative=? " +
            "WHERE landing_site_id=? AND booster_id=? AND landing_date=?";
    private final String DELETE = "DELETE FROM landing " +
            "WHERE landing_site_id=? AND booster_id=? AND landing_date=?";

    public LandingCRUD(Connection connection) {
        this.connection = connection;
    }

    @Override
    public boolean insertLanding(String landing_site_id, String booster_id, Date landing_date) {
        boolean inserted = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, landing_site_id);
            preparedStatement.setString(2, booster_id);
            preparedStatement.setDate(3, landing_date);
            if (preparedStatement.executeUpdate() == 1) inserted = true;
            preparedStatement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return inserted;
    }

    @Override
    public List<Landing> selectAllLandings() {
        List<Landing> boosters = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Landing landing = new Landing(
                        resultSet.getString("landing_site_id"),
                        resultSet.getString("booster_id"),
                        resultSet.getDate("landing_date")
                );
                boosters.add(landing);
            }
            preparedStatement.close();
            resultSet.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return boosters;
    }

    @Override
    public boolean deleteLanding(String landing_site_id, String booster_id, Date landing_date) {
        boolean deleted = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setString(1, landing_site_id);
            preparedStatement.setString(2, booster_id);
            preparedStatement.setDate(3, landing_date);
            if (preparedStatement.executeUpdate() == 1) deleted = true;
            preparedStatement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return deleted;
    }
}
