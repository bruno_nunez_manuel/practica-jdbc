package jdbc.controllers;

import jdbc.dao_interfaces.MissionDAO;
import jdbc.models.Mission;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MissionCRUD implements MissionDAO {
    private final Connection connection;
    private final String INSERT = "INSERT INTO mission (" +
            "mission_id, name, payload_kg, altitude_km, flight_date, booster_id) " +
            "VALUES (?, ?, ?, ?, ?, ?)";
    private final String SELECT = "SELECT * FROM mission";
    private final String UPDATE = "UPDATE mission " +
            "SET mission_id=?, name=?, payload_kg=?, " +
            "altitude_km=?, flight_date=?, booster_id=?" +
            "WHERE mission_id=?";
    private final String DELETE = "DELETE FROM mission WHERE mission_id=?";

    public MissionCRUD(Connection connection) {
        this.connection = connection;
    }

    @Override
    public boolean insertMission(
            int mission_id,
            String name,
            double payload_kg,
            int altitude_km,
            Date flight_date,
            String booster_id)
    {
        boolean inserted = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setInt(1, mission_id);
            preparedStatement.setString(2, name);
            preparedStatement.setDouble(3, payload_kg);
            preparedStatement.setInt(4, altitude_km);
            preparedStatement.setDate(5, flight_date);
            preparedStatement.setString(6, booster_id);
            if (preparedStatement.executeUpdate() == 1) inserted = true;
            preparedStatement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return inserted;
    }

    @Override
    public List<Mission> selectAllMissions() {
        List<Mission> missions = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Mission mission = new Mission(
                        resultSet.getInt("mission_id"),
                        resultSet.getString("name"),
                        resultSet.getDouble("payload_kg"),
                        resultSet.getInt("altitude_km"),
                        resultSet.getDate("flight_date"),
                        resultSet.getString("booster_id")
                );
                missions.add(mission);
            }
            preparedStatement.close();
            resultSet.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return missions;
    }

    @Override
    public boolean updateMissionByID(
            int mission_id,
            String name,
            double payload_kg,
            int altitude_km,
            Date flight_date,
            String booster_id)
    {
        boolean updated = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setInt(1, mission_id);
            preparedStatement.setString(2, name);
            preparedStatement.setDouble(3, payload_kg);
            preparedStatement.setInt(4, altitude_km);
            preparedStatement.setDate(5, flight_date);
            preparedStatement.setString(6, booster_id);
            preparedStatement.setInt(7, mission_id);
            if (preparedStatement.executeUpdate() == 1) updated = true;
            preparedStatement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return updated;
    }

    @Override
    public boolean deleteMissionByID(int mission_id) {
        boolean deleted = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setInt(1, mission_id);
            if (preparedStatement.executeUpdate() == 1) deleted = true;
            preparedStatement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return deleted;
    }

}
