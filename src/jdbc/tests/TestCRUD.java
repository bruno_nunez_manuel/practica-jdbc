package jdbc.tests;

import jdbc.connections.Conexion;
import jdbc.controllers.LandingCRUD;
import jdbc.dao_interfaces.LandingDAO;
import jdbc.dao_interfaces.LandingSiteDAO;
import jdbc.dao_interfaces.MissionDAO;
import jdbc.models.F9Booster;
import jdbc.models.Landing;
import jdbc.models.LandingSite;
import jdbc.models.Mission;

import java.sql.*;


public class TestCRUD {

    public static void main(String[] args) {
        String urlServer = "jdbc:mysql://localhost:3306/spacex_f9";
        String user = "root";
        String password = "1234";

        try {
            Connection connection1 = new Conexion(urlServer, user, password).getConnection();

            /*LandingSiteDAO landingSiteController = new LandingSiteCRUD(connection1);
            landingSiteController.insertLandingSite(
                    "qwe","z","land",false);
            landingSiteController.deleteLandingSiteByID("qwe");
            landingSiteController.updateLandingSiteByID("qwe","AAAAA",
                    "sea",true);
            for (LandingSite l : landingSiteController.selectAllLandingSites()) {
                System.out.println(l);
            }*/

            /*MissionDAO missionController = new MissionCRUD(connection1);
            missionController.insertMission(222, "1", 1,
                    1, Date.valueOf("2018-09-10"), "B1063");
            missionController.deleteMissionByID(222);
            missionController.updateMissionByID(222, "AAAAAAA", 1,
                    1, Date.valueOf("2018-09-10"), "B1063");
            for (Mission m : missionController.selectAllMissions()) {
                System.out.println(m);
            }*/

            /*F9_boosterDAO boosterController = new F9BoosterCRUD(connection1);
            boosterController.insertF9Booster("wew", Date.valueOf("2018-09-09"),
                    23, false);
            boosterController.deleteF9BoosterByID("wew");
            boosterController.updateF9BoosterByID("wew", Date.valueOf("2018-12-09"),
                    69, false);
            for (F9Booster b : boosterController.selectAllF9Boosters()) {
                System.out.println(b);
            }*/

            /*LandingDAO landingController = new LandingCRUD(connection1);
            landingController.insertLanding(
                    "LZ-1","B1049",Date.valueOf("2021-01-01"));
            landingController.deleteLanding(
                    "LZ-1","B1049",Date.valueOf("2021-01-01"));
            // Updatear una tabla intermedia? Noooooooooo.
            // Borras la row entera y la resinsertas porque lo digo yo.
            for (Landing l : landingController.selectAllLandings()) {
                System.out.println(l);
            }*/

            connection1.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
