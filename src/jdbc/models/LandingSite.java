package jdbc.models;


public class LandingSite {
    String landing_site_id;
    String name;
    String land_or_sea;
    boolean is_operative;

    public LandingSite(
            String landing_site_id,
            String name,
            String land_or_sea,
            boolean is_operative)
    {
        this.landing_site_id = landing_site_id;
        this.name = name;
        this.land_or_sea = land_or_sea;
        this.is_operative = is_operative;
    }

    public String getLanding_site_id() {
        return landing_site_id;
    }

    public void setLanding_site_id(String landing_site_id) {
        this.landing_site_id = landing_site_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLand_or_sea() {
        return land_or_sea;
    }

    public void setLand_or_sea(String land_or_sea) {
        this.land_or_sea = land_or_sea;
    }

    public boolean isIs_operative() {
        return is_operative;
    }

    public void setIs_operative(boolean is_operative) {
        this.is_operative = is_operative;
    }

    @Override
    public String toString() {
        return "LandingSite{" +
                "landing_site_id='" + landing_site_id + '\'' +
                ", name='" + name + '\'' +
                ", land_or_sea='" + land_or_sea + '\'' +
                ", is_operative=" + is_operative +
                '}';
    }
}
