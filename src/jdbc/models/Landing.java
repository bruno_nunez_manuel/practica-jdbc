package jdbc.models;

import java.sql.Date;

public class Landing {
    String landing_site_id;
    String booster_id;
    Date landing_date;

    public Landing(String landing_site_id, String booster_id, Date landing_date) {
        this.landing_site_id = landing_site_id;
        this.booster_id = booster_id;
        this.landing_date = landing_date;
    }

    public String getLanding_site_id() {
        return landing_site_id;
    }

    public void setLanding_site_id(String landing_site_id) {
        this.landing_site_id = landing_site_id;
    }

    public String getBooster_id() {
        return booster_id;
    }

    public void setBooster_id(String booster_id) {
        this.booster_id = booster_id;
    }

    public Date getLanding_date() {
        return landing_date;
    }

    public void setLanding_date(Date landing_date) {
        this.landing_date = landing_date;
    }

    @Override
    public String toString() {
        return "Landing{" +
                "landing_site_id='" + landing_site_id + '\'' +
                ", booster_id='" + booster_id + '\'' +
                ", landing_date=" + landing_date +
                '}';
    }
}
