package jdbc.models;

import java.sql.Date;

public class Mission {
    int mission_id;
    String name;
    double payload_kg;
    int altitude_km;
    Date flight_date;
    String booster_id;

    public Mission(
            int mission_id,
            String name,
            double payload_kg,
            int altitude_km,
            Date flight_date,
            String booster_id)
    {
        this.mission_id = mission_id;
        this.name = name;
        this.payload_kg = payload_kg;
        this.altitude_km = altitude_km;
        this.flight_date = flight_date;
        this.booster_id = booster_id;
    }

    public int getMission_id() {
        return mission_id;
    }

    public void setMission_id(int mission_id) {
        this.mission_id = mission_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPayload_kg() {
        return payload_kg;
    }

    public void setPayload_kg(double payload_kg) {
        this.payload_kg = payload_kg;
    }

    public int getAltitude_km() {
        return altitude_km;
    }

    public void setAltitude_km(int altitude_km) {
        this.altitude_km = altitude_km;
    }

    public Date getFlight_date() {
        return flight_date;
    }

    public void setFlight_date(Date flight_date) {
        this.flight_date = flight_date;
    }

    public String getBooster_id() {
        return booster_id;
    }

    public void setBooster_id(String booster_id) {
        this.booster_id = booster_id;
    }

    @Override
    public String toString() {
        return "Mission{" +
                "mission_id=" + mission_id +
                ", name='" + name + '\'' +
                ", payload_kg=" + payload_kg +
                ", altitude_km=" + altitude_km +
                ", flight_date=" + flight_date +
                ", booster_id='" + booster_id + '\'' +
                '}';
    }
}
