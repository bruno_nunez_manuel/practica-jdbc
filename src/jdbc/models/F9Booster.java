package jdbc.models;

import java.sql.Date;

public class F9Booster {
    String booster_id;
    Date first_static_fire;
    int times_flown;
    boolean is_operative;

    public F9Booster(
            String booster_id,
            Date first_static_fire,
            int times_flown,
            boolean is_operative)
    {
        this.booster_id = booster_id;
        this.first_static_fire = first_static_fire;
        this.times_flown = times_flown;
        this.is_operative = is_operative;
    }

    public String getBooster_id() {
        return booster_id;
    }

    public void setBooster_id(String booster_id) {
        this.booster_id = booster_id;
    }

    public Date getFirst_static_fire() {
        return first_static_fire;
    }

    public void setFirst_static_fire(Date first_static_fire) {
        this.first_static_fire = first_static_fire;
    }

    public int getTimes_flown() {
        return times_flown;
    }

    public void setTimes_flown(int times_flown) {
        this.times_flown = times_flown;
    }

    public boolean isIs_operative() {
        return is_operative;
    }

    public void setIs_operative(boolean is_operative) {
        this.is_operative = is_operative;
    }

    @Override
    public String toString() {
        return "F9_booster{" +
                "booster_id='" + booster_id + '\'' +
                ", first_static_fire=" + first_static_fire +
                ", times_flown=" + times_flown +
                ", is_operative=" + is_operative +
                '}';
    }
}
