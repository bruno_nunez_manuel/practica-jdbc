package jdbc.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
    private String cadena_conexion;
    private String usuario;
    private String contrasena;

    public Conexion(String cadena_conexion, String usuario, String contrasena) {
        this.cadena_conexion = cadena_conexion;
        this.usuario = usuario;
        this.contrasena = contrasena;
    }

    //método para conectarse que devolverá la conexión
    public Connection getConnection() {
        try {
            return DriverManager.getConnection(
                    this.cadena_conexion, this.usuario, this.contrasena);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    //método para desconectarse void
    public void closeConnection(Connection conn) {
        try {
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String getCadena_conexion() {
        return cadena_conexion;
    }

    public void setCadena_conexion(String cadena_conexion) {
        this.cadena_conexion = cadena_conexion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

}